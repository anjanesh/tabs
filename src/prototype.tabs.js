jscoin_tabs = Class.create(
{
    initialize: function(element)
    {
        if (Prototype.Browser.IE)
        {
            $$("#" + element + " > ul.menu > li > div").each(function(el)
            {
                $(el).style.width = $(element).getWidth() + "px";
            });
        }

        this.element = $(element);

        // http://stackoverflow.com/questions/4313243/how-to-get-child-index-of-element-in-prototype/4313319#4313319
        var activeItem = $$("#" + element + " > ul > li.active:first")[0].previousSiblings().size() + 1;

        this.element + $$(" ul > li").invoke('observe', 'click', function(event)
        {
            if (this.hasClassName("active")) return;
            var index = this.previousSiblings().size();

            // Make LI active
            $(this.parentNode).childElements()[activeItem - 1].removeClassName("active");
            this.addClassName("active");

            // Make DIV active
            $(element).select("> div")[activeItem - 1].removeClassName("active");
            $(element).select("> div")[index].addClassName("active");

            activeItem = index + 1;
        });
    }
});
