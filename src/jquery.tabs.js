(function($)
{
    $.fn.jscoin_tabs = function(options)
    {
        var defaults =
        {
            position : 'left' // left, center
        };

        var opts = $.extend(defaults, options);
        var activeItem;

        return this.each(function()
        {
            // Reference to this's jQuery object
            var $this = $(this);

            // jQuery.browser is deprecated. To be removed.
            if (jQuery.browser.msie && jQuery.browser.version <= 6)
            {
                $("> div", this).width($(this).width());
            }

            // 1st Child of tab div - should be a single UL element
            var ul = $(this).children()[0];
            if (ul.tagName.toLowerCase() != "ul")
            {
                if (window.console) console.log("First Element must be a UL");
                return false;
            }

            // Number of tabs
            var itemsLength = ul.getElementsByTagName("li").length;

            // Get active tab - search for li having class="active"
            activeItem = $("> ul > li.active:first", this).index() + 1;

            // If nothing is active, set the first one active
            if (!activeItem) $("> ul > li:nth-child(" + (activeItem = 1) + ")", this).addClass("active");

            // Center the tab elements
            if  (opts.position == "center")
             $this.find("> ul").addClass("center").end().find("> ul > li").addClass("center");

            // Make sure the right DIV is active
            $("> div", this).each(function(i)
            {
                var $this = $(this);
                if (i != activeItem - 1)
                {
                    if ($this.hasClass("active")) $this.removeClass("active");
                    return;
                }
                if (!$this.hasClass("active")) $this.addClass("active");
            });

            $("> ul > li", this).bind("click", {tab:$this}, function(event)
            {
                var $this = $(this);
                if ($this.hasClass("active")) return;
                var index = $this.index();

                // Make LI active
                $($this.parent().children()[activeItem - 1]).removeClass("active");
                $this.addClass("active");

                // Make DIV active
                $("> div:eq(" + (activeItem - 1) + ")", event.data.tab).removeClass("active");
                $("> div:eq(" + index + ")", event.data.tab).addClass("active");

                activeItem = index + 1;
            });
        });
    }
})(jQuery);
